<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
    
use App\Models\Agency as Agency;
use App\Http\Requests\AgencyCreateRequest;
use App\Http\Requests\AgencyUpdateRequest;

class AgencyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('config.agency', ['page'=>'Agencias','user' => auth()->user()->name]);
    }

    /**
     * Show the dataTable.
     *
     * @return \Illuminate\Http\Response
     */
    public function list()
    {
        $agencies = Agency::all();
        $data = [];
        foreach ($agencies as $agency) {
            $id = $agency['id'];
            $agency['buttons'] = '<div><a href="javascript:void(0)" class="btn btn-sm btn-default btn-icon btn-outline btn-round editar" data-toolbar="'.$id.'" title="Editar agencia" rol="tooltip" ><i class="fa fa-pencil" aria-hidden="true"></i></a><a href="javascript:void(0)" class="btn btn-sm btn-danger btn-icon btn-outline btn-round elimiar" data-toolbar="'.$id.'" title="Eliminar agencia" rol="tooltip" ><i class="fa fa-close" aria-hidden="true"></i></a></div>';
            $agency['estado']=$agency['agencyState'];
            array_push($data,$agency);
        }

        return response()->json(['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AgencyCreateRequest $request)
    {
        if ($request->ajax()) 
        {
            $data =[
                'nombre' => $request['txtNombre'],
                'direccion' => $request['txtDireccion'],
                'telefono' => $request['txtTelefono'],
                'agency_state_id' => 1,
                'user_cre_id' => auth()->user()->id,
            ];
            $agency = new Agency($data);
            $agency->save();

            return response()->json(['mensaje' => 'creado satisfactoriamente']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $agency = Agency::find($id);

        return response()->json($agency);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AgencyUpdateRequest $request, $id)
    {
        if ($request->ajax()) 
        {
            $data =[
                'nombre' => $request['txtNombre'],
                'direccion' => $request['txtDireccion'],
                'telefono' => $request['txtTelefono'],                
                'user_cre_mod' => auth()->user()->id,
            ];
            $agency = Agency::find($id)
                        ->update($data);

            return response()->json(['mensaje' => 'Modificado']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
