<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EventType extends Model
{
    protected $table = 'event_types';
    public $timestamps = false;

    public $fillable = [        
        "nombre",
        "nature",
        "info",
        "user_cre_id",
        "user_mod_id",
    ];
}
