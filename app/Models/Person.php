<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Person extends Model
{
    protected $table = 'people';
    const CREATED_AT = 'creation_date';
    const UPDATED_AT = 'last_update';

    public $fillable = [
        "user_id",
        "referred_id",
        "active",
        "nombre",
        "apellidos",
        "direccion",
        "email",
        "telefono",
        "celular",
        "whatsapp",
        "fec_nacimiento",
        "promotion",
        "user_cre_id",
        "user_mod_id",
    ];
}
