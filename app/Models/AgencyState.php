<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AgencyState extends Model
{
    protected $table = 'agency_states';
    public $timestamps = false;

    public $fillable = [
        "name",
    ];
}
