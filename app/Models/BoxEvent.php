<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BoxEvent extends Model
{
    protected $table = 'box_events';
    const CREATED_AT = 'creation_date';
    const UPDATED_AT = 'last_update';

    public $fillable = [
        "event_type_id",
        "especific_id",
        "agency_id",
        "box_event_state_id",
        "total",
        "nature",
        "user_cre_id",
        "user_mod_id",
    ];
}
