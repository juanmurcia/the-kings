<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VoucherState extends Model
{
 	protected $table = 'voucher_states';
 	public $timestamps = false;

    public $fillable = [
        "nombre",
    ];
}
