<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Concept extends Model
{
    protected $table = 'concepts';
    const CREATED_AT = 'creation_date';
    const UPDATED_AT = 'last_update';

    public $fillable = [
        "event_type_id",
        "active",
        "nombre",
        "service",
        "user_cre_id",
        "user_mod_id",
    ];
}
