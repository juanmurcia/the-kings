<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $table = 'services';
    const CREATED_AT = 'creation_date';
    const UPDATED_AT = 'last_update';

    public $fillable = [        
        "active",
        "nombre",
        "descripcion",
        "rate",
        "user_cre_id",
        "user_mod_id",
    ];    
}
