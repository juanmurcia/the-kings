<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VoucherDetail extends Model
{
    protected $table = 'voucher_details';
    const CREATED_AT = 'creation_date';
    const UPDATED_AT = 'last_update';

    public $fillable = [
        "voucher_id",
        "service_id",
        "rate",
        "barber_id",
        "active",        
        "user_cre_id",
        "user_mod_id",
    ];
}
