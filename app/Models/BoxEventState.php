<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class boxEventState extends Model
{
    protected $table = 'box_event_states';
    public $timestamps = false;

    public $fillable = [
        "nombre",
    ];
}
