<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Barber extends Model
{
    protected $table = 'barbers';
    const CREATED_AT = 'creation_date';
    const UPDATED_AT = 'last_update';

    public $fillable = [
        "people_id",
        "active",
        "commission",
        "agency_id",
        "user_cre_id",
        "user_mod_id",
    ];
}
