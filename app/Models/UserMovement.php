<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserMovement extends Model
{
    protected $table = 'user_movements';
    const CREATED_AT = 'creation_date';
    const UPDATED_AT = 'last_update';

    public $fillable = [
        "user_id",
        "especific_id",
        "model",
        "action",
    ];
}
