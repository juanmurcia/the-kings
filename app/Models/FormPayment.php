<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FormPayment extends Model
{
    protected $table = 'form_payments';
    public $timestamps = false;
    
    public $fillable = [
        "name",
    ];
}
