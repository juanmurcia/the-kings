<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Agency extends Model
{
    protected $table = 'agencies';

    public $fillable = [
        "agency_state_id",
        "nombre",
        "direccion",
        "telefono",
        "user_cre_id",
        "user_mod_id",
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    
    protected $casts = [
        "user_cre_id" => "integer",
        "user_mod_id" => "integer",
        "nombre" => "string|Min:4",
        "direccion" => "string|Min:4",
        "telefono" => "string|Min:4",
    ];

    public static $rules = [
        'txtNombre' => 'required',
        'txtDireccion' => 'required',
        'txtTelefono' => 'required',
    ];
    
    public static $fields = [
        'txtNombre' => 'Nombre',
        'txtDireccion' => 'Direccion',
        'txtTelefono' => 'Telefono',
    ];


    public function agencyState()
    {
        return $this->belongsTo('App\Models\AgencyState');
    }
}
