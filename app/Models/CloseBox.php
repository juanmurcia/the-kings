<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CloseBox extends Model
{
    protected $table = 'close_boxes';
    const CREATED_AT = 'creation_date';
    const UPDATED_AT = 'last_update';

    public $fillable = [
        "agency_id",
        "prevolious_balance",
        "actual_balance",
        "ttl_debit",
        "ttl_credit",
        "date_from",
        "date_to",
        "user_cre_id",
        "user_mod_id",
    ];
}
