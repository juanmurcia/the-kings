<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Voucher extends Model
{
    protected $table = 'vouchers';
    const CREATED_AT = 'creation_date';
    const UPDATED_AT = 'last_update';

    public $fillable = [
        "agency_id",
        "concept_id",
        "form_payment_id",
        "client_id",
        "total",
        "description",
        "user_cre_id",
        "user_mod_id",
    ];
}
