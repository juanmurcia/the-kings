@extends('layouts.master')

@section('content')
    <div class="right_col" role="main">
		<div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="row x_title">
                  <div class="col-md-6">
                    <h3>{{ $page }} <small>Listado usuarios registrados</small></h3>
                  </div>                  
                </div>
                <div class="clearfix"></div>
            </div>
      	</div>
		
		<div class="row">
      <table id="tbUsers" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%" style="width: 100%;">
        <thead>
            <tr>
                <th>Nombre</th>
                <th>Email</th>
                <th>Fec Cre</th>
                <th>Fec Mod</th>
            </tr>
        </thead>                
      </table>
    </div>

	</div>          
@stop

@section('style')
  <link rel="stylesheet" href="{{asset('vendors/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
  <link rel="stylesheet" href="{{asset('vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css')}}">
  <link rel="stylesheet" href="{{asset('vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css')}}">  
  <link rel="stylesheet" href="{{asset('vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css')}}">  
  <link rel="stylesheet" href="{{asset('vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css')}}">  
@stop

@section('script')
  <script src="{{asset('vendors/datatables.net/js/jquery.dataTables.min.js')}}"></script>  
  <script src="{{asset('vendors/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>  
  <script src="{{asset('vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js')}}"></script>  
  <script src="{{asset('vendors/datatables.net-keytable/js/dataTables.keyTable.min.js')}}"></script>  
  <script src="{{asset('vendors/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>  
  <script src="{{asset('vendors/datatables.net-scroller/js/dataTables.scroller.min.js')}}"></script>  
  <script src="{{asset('vendors/datatables/datatable.js')}}"></script>
  <script src="{{asset('js/components/user.js')}}"></script>
@stop