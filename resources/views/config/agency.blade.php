@extends('layouts.master')

@section('content')
    <div class="right_col" role="main">
		<div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="row x_title">
                  	<div class="col-md-6">
                    	<h1>Agencias <small>Listado agencias disponibles</small></h1>
                	</div>
                    <div class="col-md-6 btn-group float-right">
			        	<button type="button" id="btnAgency" class="btn btn-lg btn-round btn-warning" data-toggle="modal" data-target="#modAgency">
			        		<span class="fa fa-plus" aria-hidden="true"></span>
			        	</button>
					</div>                  
                </div>                
                <div class="clearfix"></div>
            </div>
      	</div>
		
		<div class="row">
			<div class="col-sm-12">
				<div class="x_content">
					<table id="tbAgencies" class="table table-striped table-hover dt-responsive nowrap" cellspacing="0" style="width: 100%;">
			            <thead>
			                <tr>
			                    <th>Nombre</th>
			                    <th>Estado</th>
			                    <th>Direccion</th>
			                    <th>Telefono</th>
			                    <th>Fec Cre</th>
			                    <th>Fec Mod</th>
			                    <th></th>
			                </tr>
			            </thead>                
			        </table>	       	
				</div>
			</div>
	    </div>
	</div>    

	<!-- Modal -->
	<div class="modal fade" id="modAgency" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
				  <button type="button" class="close" data-dismiss="modal">&times;</button>
				  <h3 class="modal-title" id="titleModal">Crear Agencia</h3>
				  <div class="clearfix"></div>
				</div>
				<div class="modal-body">
					<form action="#" class="form-horizontal form-label-left" id="frmAgency" name="frmAgency" onsubmit="return false;">						
						<div class="form-group">
	                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nombre <span class="required">*</span></label>
	                        <div class="col-md-6 col-sm-6 col-xs-12">
	                          <input type="text" id="txtNombre" name="txtNombre" required="required" class="form-control col-md-7 col-xs-12">
	                        </div>
                      	</div>
                      	<div class="form-group">
	                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Dirección <span class="required">*</span></label>
	                        <div class="col-md-6 col-sm-6 col-xs-12">
	                          <input type="text" id="txtDireccion" name="txtDireccion" required="required" class="form-control col-md-7 col-xs-12">
	                        </div>
                      	</div>
                      	<div class="form-group">
	                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Teléfono <span class="required">*</span></label>
	                        <div class="col-md-6 col-sm-6 col-xs-12">
	                          <input type="number" id="txtTelefono" name="txtTelefono" required="required" class="form-control col-md-7 col-xs-12">
	                        </div>
                      	</div>
                      	<div class="ln_solid"></div>
						<div class="form-group">
							<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 text-center">
								<button type="button" id="actionAgency" data-role="add" class="btn btn-lg btn-round btn-success">
									<i class="fa fa-check" aria-hidden="true"></i>
								</button>
								<button class="btn btn-lg btn-round btn-default" type="reset">
									<i class="fa fa-eraser" aria-hidden="true"></i>
								</button>
								<button class="btn btn-lg btn-round btn-danger" data-dismiss="modal" type="button">
									<i class="fa fa-close" aria-hidden="true"></i>
								</button>
							</div>
						</div>
						@csrf
					</form>
				</div>
			</div>
		</div>
	</div>  
@stop

@section('style')
  <link rel="stylesheet" href="{{asset('vendors/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
  <link rel="stylesheet" href="{{asset('vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css')}}">
  <link rel="stylesheet" href="{{asset('vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css')}}">  
  <link rel="stylesheet" href="{{asset('vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css')}}">  
  <link rel="stylesheet" href="{{asset('vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css')}}">  
@stop

@section('script')
  <script src="{{asset('vendors/datatables.net/js/jquery.dataTables.min.js')}}"></script>  
  <script src="{{asset('vendors/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>  
  <script src="{{asset('vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js')}}"></script>  
  <script src="{{asset('vendors/datatables.net-keytable/js/dataTables.keyTable.min.js')}}"></script>  
  <script src="{{asset('vendors/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>  
  <script src="{{asset('vendors/datatables.net-scroller/js/dataTables.scroller.min.js')}}"></script>  
  <script src="{{asset('vendors/datatables/datatable.js')}}"></script>
  <script src="{{asset('js/components/agency.js')}}"></script>
@stop