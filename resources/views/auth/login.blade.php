@extends('layouts.empty')

@section('content')
    <div class="page">
        <div class="page-login" >
            <div class="row" >
                <div  class="col-md-8 hidden-xs text-center"></div>
                <div class="col-sm-4 col-xs-10">
                    <div class="box-login col-xs-offset-1" >
                        <div class="col-md-10 col-md-offset-1" >
                            <form method="POST" action="{{ route('login') }}">
                                @csrf
                                <div class="page-brand-info center">
                                    <div class="row text-center">
                                        <img src="/images/logo.png" width="80%">
                                    </div>
                                </div>
                                <div class="row">
                                    <h3 class="font-size-24 text-center" style="color: white;">Inicio de Sesión</h3>
                                </div>
                                @if (count($errors) > 0)
                                    <div class="alert alert-danger alert-dismissible">
                                        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                                        <i class="icon fa fa-ban"></i><strong>Whoops!</strong> Hay algunos problemas en la autenticacion.<br><br>
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                                <div class="form-group">
                                    <input id="email" type="email" class="input_login{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder="Email" required autofocus>
                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <input id="password" placeholder="Password" type="password" class="input_login{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <button type="submit" style="color: white;" class="btn btn-block btn-warning btn-lg">Ingresa</button>
                                </div>
                                <div class="social-auth-links text-center">
                                    <a href="#"><p>¿Olvido su contraseña?</p></a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>  
            </div>
        </div>
    </div>
@endsection

@section('style')
    <link href="{{asset('css/login.css')}}" rel="stylesheet">
@endsection