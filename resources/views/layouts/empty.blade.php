<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="images/ico.png" type="image/ico" />
    <title>{{ config('app.name', 'Laravel') }}</title>

    @include('partials.default-header')
  </head>

  <body>      
    <!-- page content -->
      @yield('content')
    <!-- /page content -->
    @include('partials.default-footer')  
  </body>
</html>