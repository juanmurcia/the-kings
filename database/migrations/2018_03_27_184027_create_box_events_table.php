<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBoxEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('box_events', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('event_type_id')->unsigned();
            $table->integer('especific_id')->unsigned();
            $table->integer('agency_id')->unsigned();
            $table->integer('box_event_state_id')->unsigned();
            $table->integer('user_cre_id')->unsigned();
            $table->integer('user_mod_id')->unsigned();
            $table->string('nature');
            $table->decimal('total')->comment('Valor del evento generado segun el id especifico');
            $table->timestamps();
             //Claves
            $table->foreign('user_cre_id')->references('id')->on('users');
            $table->foreign('user_mod_id')->references('id')->on('users');
            $table->foreign('agency_id')->references('id')->on('agencies');
            $table->foreign('event_type_id')->references('id')->on('event_types');
            $table->foreign('box_event_state_id')->references('id')->on('box_event_states');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('box_events');
    }
}
