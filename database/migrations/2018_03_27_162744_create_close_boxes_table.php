<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCloseBoxesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('close_boxes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('num_voucher');
            $table->integer('user_cre_id')->unsigned();
            $table->integer('user_mod_id')->unsigned();
            $table->integer('agency_id')->unsigned();
            $table->date('date_from');
            $table->date('date_to');
            $table->decimal('prevolious_balance')->comment('Valor del cierre de caja anterior');
            $table->decimal('actual_balance')->comment('Valor del cierre actual');
            $table->timestamps();
            //Claves
            $table->foreign('user_cre_id')->references('id')->on('users');
            $table->foreign('user_mod_id')->references('id')->on('users');
            $table->foreign('agency_id')->references('id')->on('agencies');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('close_boxes');
    }
}
