<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVouchersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vouchers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('num_voucher');
            $table->integer('user_cre_id')->unsigned();
            $table->integer('user_mod_id')->unsigned();
            $table->integer('agency_id')->unsigned();
            $table->integer('concept_id')->unsigned();
            $table->integer('form_payment_id')->unsigned();
            $table->integer('voucher_state_id')->unsigned();
            $table->string('nombre');
            $table->string('description');
            $table->decimal('ttl_rate')->comment('Totalizador del campo rate del detalle');
            $table->decimal('ttl_dto')->comment('Totalizador del campo dto del detalle');
            $table->decimal('ttl_balance')->comment('Totalizador del campo balance del detale');
            $table->boolean('active');
            $table->timestamps();
             //Claves
            $table->foreign('user_cre_id')->references('id')->on('users');
            $table->foreign('user_mod_id')->references('id')->on('users');
            $table->foreign('agency_id')->references('id')->on('agencies');
            $table->foreign('concept_id')->references('id')->on('concepts');
            $table->foreign('form_payment_id')->references('id')->on('form_payments');
            $table->foreign('voucher_state_id')->references('id')->on('voucher_states');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vouchers');
    }
}
