<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBarbersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('barbers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('people_id')->unsigned();
            $table->integer('agency_id')->unsigned();
            $table->boolean('active');
            $table->decimal('commission');
            $table->integer('user_cre_id')->unsigned();
            $table->integer('user_mod_id')->unsigned()->nullable();
            $table->timestamps();
            //Claves
            $table->foreign('people_id')->references('id')->on('people');
            $table->foreign('agency_id')->references('id')->on('agencies');
            $table->foreign('user_cre_id')->references('id')->on('users');
            $table->foreign('user_mod_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('barbers');
    }
}
