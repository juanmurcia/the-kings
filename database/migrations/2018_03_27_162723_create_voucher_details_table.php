<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVoucherDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('voucher_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_cre_id')->unsigned();
            $table->integer('user_mod_id')->unsigned();
            $table->integer('voucher_id')->unsigned();
            $table->integer('service_id')->unsigned();
            $table->integer('barber_id')->unsigned();
            $table->decimal('rate');
            $table->decimal('dto');
            $table->decimal('balance');
            $table->boolean('active');
            $table->timestamps();
             //Claves
            $table->foreign('user_cre_id')->references('id')->on('users');
            $table->foreign('user_mod_id')->references('id')->on('users');
            $table->foreign('voucher_id')->references('id')->on('vouchers');
            $table->foreign('service_id')->references('id')->on('services');
            $table->foreign('barber_id')->references('id')->on('barbers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('voucher_details');
    }
}
