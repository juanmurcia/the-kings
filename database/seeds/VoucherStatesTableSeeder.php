<?php

use Illuminate\Database\Seeder;
use App\Models\VoucherState;

class VoucherStatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $voucher_state = new VoucherState();
        $voucher_state->name = 'Generado';
        $voucher_state->save();
        //
        $voucher_state = new VoucherState();
        $voucher_state->name = 'Anulado';
        $voucher_state->save();
        //
        $voucher_state = new VoucherState();
        $voucher_state->name = 'Pendiente';
        $voucher_state->save();
    }
}
