<?php

use Illuminate\Database\Seeder;
use App\Models\EventType;

class EventTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $evento_type = new EventType();
        $evento_type->name = 'Ingreso';
        $evento_type->nature = 'D';
        $evento_type->info = FALSE;
        $evento_type->save();
        //
        $evento_type = new EventType();
        $evento_type->name = 'Salida';
        $evento_type->nature = 'C';
        $evento_type->info = FALSE;
        $evento_type->save();
        //
        $evento_type = new EventType();
        $evento_type->name = 'Consignacion';
        $evento_type->nature = 'C';
        $evento_type->info = FALSE;
        $evento_type->save();
        //
        $evento_type = new EventType();
        $evento_type->name = 'Pago Empleado';
        $evento_type->nature = 'C';
        $evento_type->info = FALSE;
        $evento_type->save();
        //
        $evento_type = new EventType();
        $evento_type->name = 'Informativo Salida';
        $evento_type->nature = 'C';
        $evento_type->info = TRUE;
        $evento_type->save();
        //
        $evento_type = new EventType();
        $evento_type->name = 'Informativo Ingreso';
        $evento_type->nature = 'D';
        $evento_type->info = TRUE;
        $evento_type->save();
    }
}
