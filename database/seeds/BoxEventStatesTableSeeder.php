<?php

use Illuminate\Database\Seeder;
use App\Models\BoxEventState;

class BoxEventStatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	//
        $box_event_state = new BoxEventState();
        $box_event_state->name = 'Abierto';
        $box_event_state->save();
        //
        $box_event_state = new BoxEventState();
        $box_event_state->name = 'Cerrado';
        $box_event_state->save();

    }
}
