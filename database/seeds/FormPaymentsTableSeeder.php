<?php

use Illuminate\Database\Seeder;
use App\Models\FormPayment;

class FormPaymentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $form_payment = new FormPayment();
        $form_payment->name = 'Efectivo';
        $form_payment->save();
        //
        $form_payment = new FormPayment();
        $form_payment->name = 'Baucher';
        $form_payment->save();
    }
}
