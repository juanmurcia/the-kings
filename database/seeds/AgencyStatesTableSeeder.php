<?php

use Illuminate\Database\Seeder;
use App\Models\AgencyState;

class AgencyStatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $agency_state = new AgencyState();
        $agency_state->name = 'Abierta';
        $agency_state->save();
        //
        $agency_state = new AgencyState();
        $agency_state->name = 'Cerrada';
        $agency_state->save();
        //
        $agency_state = new AgencyState();
        $agency_state->name = 'Sin Servicio';
        $agency_state->save();
    }
}
