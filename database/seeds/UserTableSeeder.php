<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Models\Role;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_admin = Role::where('name', 'admin')->first();

        $user = new User();
        $user->name = 'juanMurcia';
        $user->email = 'juanmurcia.1402@gmail.com';
        $user->password = bcrypt('1026575225');
        $user->save();
        $user->roles()->attach($role_admin);

    }
}
