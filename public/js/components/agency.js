$( document ).ready(function() {

    let obj = {
        id:'tbAgencies',
        type:'GET',
        url:'/agencies',
        columns: [
            {data: 'nombre', name: 'nombre'},            
            {data: 'estado.name', name: 'estado'},
            {data: 'direccion', name: 'direccion'},
            {data: 'telefono', name: 'telefono'},
            {data: 'created_at', name: 'fec_cre'},
            {data: 'updated_at', name: 'fec_mod'},
            {data: 'buttons', name: 'buttons', orderable: false, searchable: true}
        ]
    };
    tableSearch(obj);

    //Funciones Botones
    $('#btnAgency').click(function() {
        $('#frmAgency')[0].reset();
        $('#actionAgency').data('role','add');
        $('#titleModal').text('Crear Agencia ');
    });

    $('#actionAgency').click(function() {
        var role = $(this).data('role');

        if(role == 'add'){
            var objButton = {
                type: 'POST',
                url: '/agency',
                form: 'frmAgency',
                success: 'Registro creado satisfactoriamente',                        
                retorna: true
            };

            requestAjax(objButton);
        }else if(role== 'change'){
            var id = $(this).data('ref');

            var objButton = {
                type: 'PUt',
                url: '/agency/'+id,
                form: 'frmAgency',
                success: 'Registro Modificado satisfactoriamente',
                retorna: true
            };

            requestAjax(objButton);
        }
    });

    setTimeout(function(){
        $('.editar').click(function() {
            var id = $(this).data('toolbar');

            var objButton = {
                type: 'GET',
                url: '/agency/'+id,
                form: 'frmAgency',
                success: 'Cargado ...',                        
                retorna: true
            };

            var response = requestAjax(objButton);

            //Carga Formulario
            $('#actionAgency').data('role','change');
            $('#actionAgency').data('ref',id);
            $('#titleModal').text('Modificar Agencia '+response.nombre);
            $('#txtNombre').val(response.nombre);
            $('#txtDireccion').val(response.direccion);
            $('#txtTelefono').val(response.telefono);

            $('#modAgency').modal({show: 'true'});
        })

        $('.eliminar').click(function() {
            var personaje = $(this).data('toolbar');
            window.location.href = "/admin/entidades/"+personaje+"/delete";
        })
    },1000);
});