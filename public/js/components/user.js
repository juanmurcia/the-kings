$( document ).ready(function() {

    let obj = {
        id:'tbUsers',
        type:'GET',
        url:'/users',
        columns: [
            {data: 'name', name: 'nombre'},            
            {data: 'email', name: 'email'},
            {data: 'created_at', name: 'fec_cre'},
            {data: 'updated_at', name: 'fec_mod'}
            //{data: 'buttons', name: 'buttons', orderable: false, searchable: true}
        ]
    };
    tableSearch(obj);

    /*
    setTimeout(function(){
        $('.editar').click(function() {
            var personaje = $(this).data('toolbar');
            window.location.href = "/admin/entidades/"+personaje+"/edit";
        })

        $('.eliminar').click(function() {
            var personaje = $(this).data('toolbar');
            window.location.href = "/admin/entidades/"+personaje+"/delete";
        })
    },1000);
    */
});