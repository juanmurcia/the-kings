//Toastr para los mensajes
toastr.options = {
	"closeButton": true,
	"debug": false,
	"newestOnTop": true,
	"progressBar": true,
	"positionClass": "toast-top-left",
	"preventDuplicates": true,
	"showDuration": "1000",
	"hideDuration": "1000",
	"timeOut": "5000",
	"extendedTimeOut": "1000",
	"showEasing": "linear",
	"hideEasing": "linear",
	"showMethod": "fadeIn",
	"hideMethod": "fadeOut"
}



/*
	* Input:
	#@obj: Variable de tipo Object
    - type : Accion que va a realizar (Obligatorio)
    - url : Url donde se va a realizar la peticion ajax (Obligatorio)
    - form : Formulario desde donde se van a extraer los campos (Obligatorio)
    - resetForm (true|false) : Indica si resetea el formulario
    - cache :(true|false) Indica si la consulta va a ser cacheada en el navegador (No obligatorio)
    - success : Mensaje a mostrar si la transaccion es satisfactoria (No obligatoria)
    - funcion : Funciona a llamar si la peticion es satisfactoria (No)
    - div : Div donde se encuentra el formulario (No)
    - hide :(true|false) Indica si oculta el div donde se encuentra el formulario (No)
    - retorna : Indica si retorna la respuesta del servidor
*/

function requestAjax(obj){
	
    /**
    * Convierto los campos en mayuscula antes de enviarlos a las validaciones
    */
    $('#'+obj.form+' input[type=text]').val (function () 
    {
        return this.value.toUpperCase().trim();
    });

    var result='';
    var jsonResult='';
    $.ajax({
        type:obj.type,
        url:obj.url,
        cache: ((obj.hasOwnProperty('cache')) ? obj.cache : false),
        data:$('#'+obj.form).serialize(),
        dataType: 'json',
        async: false,
        success: function(rta){
            if(typeof rta != 'undefined')
            {
                if(rta.ErrorBd!=null){//Si existe erro de la base de datos muestro el mensaje                
                    toastr["error"](rta.ErrorBd, rta.TlErrorBd);
                }else if(rta.Error){
                    toastr["error"](rta.ErrorPeril, rta.TlErrorPeril);
                    window.location.reload(true);
                }else{
                    //Si debe mostrar un mensaje de respuesta satisfactoria
                    if(obj.hasOwnProperty('success')){
                        toastr["success"](obj.success, '');
                    }
                    //Ocultar el div donde se encuentra el formulario
                    if(obj.hasOwnProperty('div') && obj.hasOwnProperty('hide') && obj.hide==true){
                        ocultar_div('#'+obj.div);
                    }

                    //Verifica si debe llamar a una funcion adicional
                    if(obj.hasOwnProperty('funcion')){
                        var fn = window[obj.funcion];
                        fn(rta.mensaje);
                    }

                    //Resetear el formulario
                    if(obj.hasOwnProperty('form') && obj.hasOwnProperty('resetForm') && obj.resetForm==true){
                       $('#'+obj.form).trigger('reset');
                    }

                    //Click boton, para que busque una vez se modifique un registro
                    if(obj.hasOwnProperty('clickButton') && obj.hasOwnProperty('clickButton')){
                        $("#"+obj.clickButton).trigger( "click" );
                    }
                }				
                result = rta;
	    }else{
                toastr["error"]("No Existe Respuesta Json!!", "");
            }
        },
        error: function(data){
        //Procesa si genero errores la peticion ajax.
            var errors = $.parseJSON(data.responseText);      
            $.each(errors, function(index, value) {
                toastr["error"](value, "");
            });
        }
    });	
    if(obj.retorna==true)return result;
}