/*!
 * remark (http://getbootstrapadmin.com/remark)
 * Copyright 2017 amazingsurge
 * Licensed under the Themeforest Standard Licenses
 */
(function(document, window, $) {
  'use strict';

  var Site = window.Site;

  $(document).ready(function($) {
    $.fn.dataTable.ext.type.order['salary-grade-pre'] = function ( d ) {
        switch ( d ) {
            case 'Low':    return 1;
            case 'Medium': return 2;
            case 'High':   return 3;
        }
        return 0;
    };

    Site.run();
  });
});

function numberFormat( number, decimals, dec_point, thousands_sep ) {
    // http://kevin.vanzonneveld.net
    // +   original by: Jonas Raoni Soares Silva (http://www.jsfromhell.com)
    // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // +     bugfix by: Michael White (http://getsprink.com)
    // +     bugfix by: Benjamin Lupton
    // +     bugfix by: Allan Jensen (http://www.winternet.no)
    // +    revised by: Jonas Raoni Soares Silva (http://www.jsfromhell.com)
    // +     bugfix by: Howard Yeend
    // +    revised by: Luke Smith (http://lucassmith.name)
    // +     bugfix by: Diogo Resende
    // *     example 1: number_format(1234.56);
    // *     returns 1: '1,235'
    // *     example 2: number_format(1234.56, 2, ',', ' ');
    // *     returns 2: '1 234,56'
    // *     example 3: number_format(1234.5678, 2, '.', '');
    // *     returns 3: '1234.57'
    // *     example 4: number_format(67, 2, ',', '.');
    // *     returns 4: '67,00'
  
    var n = number, prec = decimals, dec = dec_point, sep = thousands_sep;
    n = !isFinite(+n) ? 0 : +n;
    prec = !isFinite(+prec) ? 0 : Math.abs(prec);
    sep = sep == undefined ? ',' : sep;
  
    var s = n.toFixed(prec),
  abs = Math.abs(n).toFixed(prec),
  _, i;
  
    if (abs > 1000) {
        _ = abs.split(/\D/);
        i = _[0].length % 3 || 3;
    
        _[0] = s.slice(0,i + (n < 0)) +
    _[0].slice(i).replace(/(\d{3})/g, sep+'$1');
    
        s = _.join(dec || '.');
    } else {
        s = abs.replace('.', dec_point);
    }
  
    return s;
}

function tableSearch(obj){
  var oTable = $('#'+obj.id).DataTable({
      colReorder: true,
      responsive: true,
      processing: true,    
      language: {
        'url': "vendors/datatables/Spanish.json"
      },
      serverSide: false,
      searching: true,
      destroy: (obj.destroy ? true : false),
      ajax: {
          type: obj.type,
          url: obj.url,
          data: function (d) {
              $("#"+obj.form).find(':input').each(function() {
                  var element= this;
                  if(element.type == 'checkbox'){
                    if(element.name != ''){
                      d[element.name] = element.checked;
                    }
                  }else{
                    if(element.name != '' && element.value != ''){
                      d[element.name] = element.value;
                    }  
                  }
              });
          }
      },
      columns: obj.columns,
      lengthMenu: ((obj.hasOwnProperty('lengthMenu')) ? obj.lengthMenu : [[10,25,50,100, 1000], [10,25,50, 100, 1000]]),
      createdRow: (obj.hasOwnProperty('createdRow') ? obj.createdRow : ''),
      "footerCallback": function ( row, data, start, end, display ) {
          if(obj.hasOwnProperty('colTotal')){
              var api = this.api(), data;

              var intVal = function ( i ) {
                  return typeof i === 'string' ?
                      i.replace(/[\$,]/g, '')*1 :
                      typeof i === 'number' ?
                          i : 0;
              };
              $.each(obj.colTotal, function (key, value) {
                      var totalCol= api
                     .column( value[0] , { page: 'current'} )
                     .data()
                     .reduce( function (a, b) {
                         return intVal(a) + intVal(b);
                     }, 0 );
                     
                     var htmlTotal=(value[1]=='t') ? numberFormat(totalCol, 0, ',', '.'):totalCol;
                     $( api.column( value[0] ).footer() ).html(htmlTotal);
              });
          }
      }
      
  });

  $("#"+obj.form).on('submit', function(e) {    
      oTable.draw();    
      //Para que NO borre el filtro del formulario
      e.preventDefault();
  });

  return oTable;
}
